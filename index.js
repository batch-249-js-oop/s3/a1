
//act1
// 1. classes or constructor/prototype
// 2. first letter is upper cased / pascal casing 
//3. new
//4. instantiation
//5. constructor

//act2
// 1. no
// 2. no
// 3. y
// 4. getter setter
// 5. return this;



class Student {
    constructor(name,email,grades){
        this.name = name;
        this.email = email;
        this.passed = undefined;
        this.passWithHonors = undefined;
        this.gradeAve = undefined;
        if(grades.constructor===Array && grades.length===4){
            grades.every(grade => {
                return (grade >=0 && grade <= 100) ? this.grades = grades : this.grades = undefined;
            })
        } else {
            this.grades = undefined;
        }
    }

    login(){
        console.log(this.email, `has logged in`);
        return this;
    }
    
    logout(){
        console.log(this.email, `has logged out`);
        return this;
    }
    listGrades(){
        console.log(this.name, `quarterly grades are: `, this.grades);
        return this;
    }
    computeAve(){
        this.gradeAve = this.grades.reduce((previousGrade, currentGrade) =>{
            return previousGrade + currentGrade;
        })/(this.grades.length);
        
        return this;
    }
    willPass(){
        if(this.computeAve() >= 85){
            this.passed = true;
        } else {
            this.passed = false;
        }
        return this;
    }

    willPassWithHonors(){
        if(this.willPass()){
            (this.computeAve()>=90)? this.passWithHonors = true : this.passWithHonors = false;
        }
        return this;
    }
}

const studentOne = new Student ('John', 'john@mail.com', [89,84,78,88]);
const studentTwo = new Student ('Joe', 'joe@mail.com', [78, 82, 79, 85]);
const studentThree = new Student ('Jane', 'jane@mail.com', [87,89,91, 93]);
const studentFour = new Student ('Jessie', 'jessie@mail.com', [91,89,92,93]);

let test = studentOne.login().computeAve().willPass().willPassWithHonors().logout();
console.log(test)